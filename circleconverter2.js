input = 0

let circumference = (input) => {
    return 2*3.1416*input
}

let area = (input) => {
    return 3.1416*input**2
}

input = Number(prompt("Circle calculator.\nEnter a radius:"))

document.getElementById("output").innerHTML = `Circle circumference ${circumference(input)}\nCircle area: ${area(input)}`



