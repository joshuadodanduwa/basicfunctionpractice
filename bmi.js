let answer = (weight, height) => weight / (height / 100) ** 2

let category = (bmi) => {
    if (bmi <18.5) {
        return "underweight"                                              
    } else if (bmi >= 18.5 && bmi < 25) {
        return "normal weight"                                         
    } else if (bmi >= 25 && bmi < 30) {
        return "overweight"                                     
    } else {
        return "obese"                                      
    }
}

let inputkg = 0
let inputcm = 0
let bodymassindex = 0
let bmicategory = ""

inputkg = Number(prompt("Body Mass Index (BMI) Calculator.\nEnter your weight (kg):"))   
inputcm = Number(prompt("Body Mass Index (BMI) Calculator.\nEnter your height (cm):"))     


bodymassindex = answer(inputkg, inputcm)
bmicategory = category(bodymassindex)

console.log(`your weight: ${inputkg} kg`)
console.log(`your height: ${inputcm} cm`)
console.log(`Your BMI: ${bodymassindex.toFixed(2)}`)
console.log(`BMI Category: ${bmicategory}`)