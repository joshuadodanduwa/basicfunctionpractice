let calculator = (num1, num2, operator) => {
    
    switch (operator){
        case "+":
            return num1 + num2      
            break
        case "-":
            return num1 - num2      
            break
        case "*":
            return num1 * num2     
            break
        case "/":
            return num1 / num2      
            break
        default:
            return "?"            
    }         
}

let input1
let input2
let op

input1 = Number(prompt("Calculator.\nEnter the first number:"))
input2 = Number(prompt("Calculator.\nEnter the second number:"))
op = prompt("Calculator.\nEnter the operator (+, -, *, /):")

console.log(`${input1} ${op} ${input2} = ${calculator(input1, input2, op)}`)